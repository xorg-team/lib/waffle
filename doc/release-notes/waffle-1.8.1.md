
The Waffle bugfix release 1.8.1 is now available.

What is new in this release:
  - Fix the meson build to avoid (over)linking against dependencies
  - Reinstate support for wayland/wayland-scanner older than 1.20
  - Run the clang-format CI stage as applicable, on the correct commits

The source is available at:

  https://waffle.freedesktop.org/files/release/waffle-1.8.1/waffle-1.8.1.tar.xz

You can also get the current source directly from the git
repository. See https://waffle.freedesktop.org for details.

----------------------------------------------------------------

Changes since 1.8.0:

Emil Velikov (11):
      release: Publish 1.8.0
      waffle: Bump post-release version to 1.8.90
      editorconfig: expand meson files, handle yml
      gitlab-ci: run clang-format only on MRs
      clang-format: correctly track the commit range in CI
      clang-format: pass correct reference to git-clang-format
      Revert "waffle: Bump post-release version to 1.8.90"
      meson: properly set the partial_dependency(es)
      clang-format: fix the post-merge commit range
      wayland: restore support for pre wayland 1.20
      waffle: Bump version to 1.8.1

